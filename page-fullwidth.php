<?php

/*Template name:Full width*/

get_header();
while (have_posts()):the_post();

    $value = get_post_meta(get_the_ID(), 'header_banner', true);

endwhile; ?>

<?php if ($value == 1): ?>

    <!--SECTION PAGE HEADER START-->

    <section id="section-page-header" class="page-blog">
        <div class="overlay black"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-header text-center">
                        <?php while (have_posts()):the_post(); ?>
                            <h3><?php the_title(); ?></h3>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>


    <!--SECTION BLOG  START-->

        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <?php while (have_posts()):the_post() ?>

                        <?php the_content(); ?>

                    <?php endwhile; ?>
                </div>

            </div>
        </div>

    <!--SECTION BLOG END-->

<?php get_footer(); ?>