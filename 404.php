<?php get_header(); ?>

    <!--SECTION 404 START-->
    <section id="main-container" class="bizcafe-content-padding">
        <div class="container">
            <div class="row">
                <div class="error-page text-center">
                    <div class="error-code">
                        <strong>404</strong>
                    </div>
                    <div class="error-message">
                        <h3>Oops... Page Not Found!</h3>
                    </div>
                    <div class="error-body">
                        Try using the button below to go to main page of the site <br>
                        <a href="<?php bloginfo('home'); ?>" class="btn btn-primary solid blank"><i class="fa fa-arrow-circle-left">&nbsp;</i> Go to Home</a>
                    </div>
                </div>
            </div>
            <!-- Content row -->
        </div>
        <!-- Conatiner end -->
    </section>
    <!-- Main container end -->
    <!--SECTION 404 END-->

    <!--SECTION FOOTER START-->
<?php get_footer(); ?>

