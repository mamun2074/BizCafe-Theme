

<!--SECTION FOOTER START-->
<section id="section-footer" class="bizcafe-content-padding black-bg">
    <div class="container">
        <div class="row">

            <?php dynamic_sidebar('footer'); ?>


        </div>
    </div>
</section>
<!--SECTION FOOTER END-->

<!--SECTION FOOTER BOTOTM START-->
<section id="section-footer-btm" class="pure-black-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>Copyright @ 2016 BIZ<span>CAFE</span> Theme. All Right Reserved</p>
            </div>
            <div class="col-md-6 text-right">
                <ul class="list-inline footer-menu">
                    <li><a href="#">Investors</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Event</a></li>
                    <li><a href="#">Disclaimer</a></li>
                    <li><a href="#">Contact us</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--SECTION FOOTER BOTOTM END-->



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwIQh7LGryQdDDi-A603lR8NqiF3R_ycA"></script>
<!--  THEME Script js  -->
<script src="<?php echo get_template_directory_uri(); ?>/"></script>

<?php wp_footer(); ?>
</body>


<!-- Mirrored from themeturn.com/pro/db/biz/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 May 2017 08:58:18 GMT -->
</html>