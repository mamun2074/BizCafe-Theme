<?php


register_sidebar(array(
    'id' => 'sidebar',
    'name' => 'Page sidebar',
    'description' => 'You may add your widget heare',
    'before_widget'=>'<div class="widget mmm">',
    'after_widget'=>'</div>',
    'before_title'=>'<h6 class="widget-title">',
    'after_title'=>'</h6>',
));

register_sidebar(array(
    'id' => 'footer',
    'name' => 'Footer widget',
    'description' => 'You may add your footer widget here',
    'before_widget'=>'<div class="col-md-3 col-sm-6"><div class="footer-widget">',
    'after_widget'=>'</div></div>',
    'before_title'=>'<h4 class="widget-title">',
    'after_title'=>'</h4>',
));