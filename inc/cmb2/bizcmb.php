<?php

function bizcafe_cmb2()
{

    /*Banner*/

    $header_banner = new_cmb2_box(array(
        'id' => 'banner',
        'title' => ' Page Header',
        'object_types' => array('page'),
    ));
    $header_banner->add_field(array(
        'name' => 'Banner',
        'desc' => 'When switch on then banner show in your page and off then hide',
        'id' => 'header_banner',
        'type' => 'select',
        'options' => array(
            '1' => 'On',
            '0' => 'Off',
        ),
    ));


    /*Portfolio*/

    $porasdf=new_cmb2_box(array(
        'id'=>'bizportf',
        'title'=>' Position ',
        'object_types'=>array('bizportfolio'),
    ));
    $porasdf->add_field(array(
        'id'=>'portfoliodesignation',
        'name'=>'Designation',
        'desc'=>'Write your designation',
        'type'=>'text',

    ));

    /*Testimonial*/

    $biztestimonial=new_cmb2_box(array(
        'id'=>'biztesti',
        'title'=>'Testimonial',
        'object_types'=>array('testimonial'),
    ));
    $biztestimonial->add_field(array(
        'id'=>'bizcafe_testimonial',
        'name'=>'Designation',
        'type'=>'text',
        'desc'=>'Write your designation',
    ));

    /*our team*/

    $bizteam=new_cmb2_box(array(
        'id'=>'bizteam',
        'title'=>'Our team',
        'object_types'=>array('ourteam'),
    ));
//    Designation
    $bizteam->add_field(array(
        'id'=>'bizcafeteamdesig',
        'name'=>'Designation',
        'type'=>'text',
        'desc'=>'Write team member designation',
    ));
//    Facebook
    $bizteam->add_field(array(
        'id'=>'bizcafeteamfb',
        'name'=>'Facebook link',
        'type'=>'text',
        'desc'=>'Write your facebook link like http://www.facebook.com',
    ));
//    twitter
    $bizteam->add_field(array(
        'id'=>'bizcafeteamtw',
        'name'=>'Twitter link',
        'type'=>'text',
        'desc'=>'Write your twitter link like http://www.twitter.com',
    ));
//    Google pluse
     $bizteam->add_field(array(
        'id'=>'bizcafeteamglp',
        'name'=>'GooglePluse link',
        'type'=>'text',
        'desc'=>'Write your google link like http://www.twitter.com',
    ));
//    Facebook










}

add_action('cmb2_admin_init', 'bizcafe_cmb2');