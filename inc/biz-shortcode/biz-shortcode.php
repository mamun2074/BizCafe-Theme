<?php

/*portfolio */

function biz_portfolio_shortcode($atts){

    $value=shortcode_atts(array(
        'number'=>8,
    ),$atts);

    ?>
    <section id="section-portflio">
        <div class="container-fluid">
            <div class="row">

                <?php
                $portfolioshort=new WP_Query(array(
                    'post_type'=>'bizportfolio',
                    'posts_per_page'=>$value['number'],
                ));

                while($portfolioshort->have_posts()):$portfolioshort->the_post(); ?>
                <div class="col-md-3 col-sm-6 portfolio-content-block">
                    <a href="<?php the_post_thumbnail_url(); ?>" class="hover-overlay work-popup">
                        <figure class="overlay-effect">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
                        </figure>
                        <div class="portfolio-caption">
                            <h5><?php echo get_post_meta(get_the_ID(),'portfoliodesignation',true) ?></h5>
                            <p>Business/ Marketing</p>
                        </div>
                    </a>
                </div>
                <?php endwhile; ?>

            </div>
        </div>
    </section>

<?php }
add_shortcode('bizcafeportfolioshortcode','biz_portfolio_shortcode');

/*Clinet satisfication counter*/

function biz_clintsatisfaction_shortcode($atts){

    $bizcount=shortcode_atts(array(
        'satisfied_counter'=>8347,
        'satisfied_clint_text'=>'Satisfied Clients',
        'monthly_counter'=>7234,
        'monthly_counter_text'=>'Monthly Earning ',
        'professonal_counter'=> 9373 ,
        'professonal_text'=> 'Professional Results',
        'project_complete_counter'=>6562,
        'project_complete_text'=>'Project Completed',
    ),$atts);
    ?>

    <section id="section-counter">
        <div class="overlay feature"></div>
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-3">
                    <span class="counter"><?php echo $bizcount['satisfied_counter']; ?></span>
                    <p><?php echo $bizcount['satisfied_clint_text']; ?> </p>
                </div>

                <div class="col-md-3 col-sm-3">
                    <span class="counter"><?php echo $bizcount['monthly_counter']; ?></span>
                    <p><?php echo $bizcount['monthly_counter_text']; ?> </p>
                </div>

                <div class="col-md-3 col-sm-3">
                    <span class="counter"> <?php echo $bizcount['professonal_counter']; ?></span>
                    <p> <?php echo $bizcount['professonal_text']; ?></p>
                </div>

                <div class="col-md-3 col-sm-3">
                    <span class="counter"><?php echo $bizcount['project_complete_counter']; ?></span>
                    <p> <?php echo $bizcount['project_complete_text']; ?></p>
                </div>

            </div>
        </div>
    </section>

    <?php
}
add_shortcode('biz_counter','biz_clintsatisfaction_shortcode');


/*Our team*/

function bizourteamshort($atts){

    $bizourteam=shortcode_atts(array(
            'team_header'=>'team',
            'team_title'=>'MEET OUR TEAM',
            'team_description'=>'Wonderful serenity has taken possession of my entire soul, like these sweet mornings',
            'post_per_page'=>3,


    ),$atts);

    ?>

    <section id="section-team" class="bizcafe-content-padding ">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center">
                        <span><?php echo $bizourteam['team_header']; ?></span>
                        <h2><?php echo $bizourteam['team_title']; ?></h2>
                        <hr>
                        <p><?php echo $bizourteam['team_description']; ?></p>
                    </div>
                </div>
            </div>

            <div class="row">

                <?php

                $bizourteamaa=new WP_Query(array(
                    'post_type'=>'ourteam',
                    'postst_per_page'=>$bizourteam['post_per_page'],
                ));


                while ($bizourteamaa->have_posts()):$bizourteamaa->the_post(); ?>
                <div class="col-md-4 col-sm-4 aos-item" data-aos="fade-up">
                    <div class="team-content-block">
                        <div class="team-inner-content">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive center-block">

                            <div class="team-hover-content">
                                <h4><?php the_title(); ?> <span><?php echo  get_post_meta(get_the_ID(),'bizcafeteamdesig',true); ?></span></h4>
                                <p><?php echo wp_trim_words(get_the_content(),15,false) ?> </p>
                                <ul class="list-inline team-social">

                                    <li><a href="<?php echo get_post_meta(get_the_ID(),'bizcafeteamfb',true); ?>"><i class="fa fa-facebook"></i></a></li>

                                    <li><a href="<?php echo get_post_meta(get_the_ID(),'bizcafeteamtw',true); ?>"><i class="fa fa-twitter"></i></a></li>

                                    <li><a href="<?php echo get_post_meta(get_the_ID(),'bizcafeteamglp',true); ?>"><i class="fa fa-google-plus"></i></a></li>

                                </ul>
                            </div>
                        </div>

                        <div class="team-content">
                            <h5><?php the_title(); ?> <span><?php echo  get_post_meta(get_the_ID(),'bizcafeteamdesig',true); ?></span></h5>
                            <ul class="list-inline team-social">

                                <li><a href="<?php echo get_post_meta(get_the_ID(),'bizcafeteamfb',true); ?>"><i class="fa fa-facebook"></i></a></li>

                                <li><a href="<?php echo get_post_meta(get_the_ID(),'bizcafeteamtw',true); ?>"><i class="fa fa-twitter"></i></a></li>

                                <li><a href="<?php echo get_post_meta(get_the_ID(),'bizcafeteamglp',true); ?>"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <?php endwhile; ?>

            </div>
        </div>
    </section>



    <?php
}
add_shortcode('bizourteam','bizourteamshort');

/*Lates post area*/

function bizlatestpost_shortcode($atts){

    $bizshortlatest=shortcode_atts(array(
        'team_header'=>'News',
        'team_title'=>'LATEST BLOG POST',
        'team_description'=>'Wonderful serenity has taken possession of my entire soul, like these sweet mornings',
        'post_per_page'=>3,


    ),$atts);

    ?>
    <section id="section-blog" class="bizcafe-content-padding grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center">
                        <span><?php echo $bizshortlatest['team_header']; ?></span>
                        <h2><?php echo $bizshortlatest['team_title']; ?></h2>
                        <hr>
                        <p><?php echo $bizshortlatest['team_description']; ?></p>
                    </div>
                </div>
            </div>

            <div class="row">

                <?php

                $bizomeblog=new WP_Query(array(
                        'post_type'=>'post',
                    'posts_per_page'=>$bizshortlatest[post_per_page],
                ));

                while($bizomeblog->have_posts()):$bizomeblog->the_post(); ?>

                <div class="col-md-4 ">
                    <div class="blog-widget ">
                        <div class="inner-box box-col">
                            <div class="clearfix">
                                <div class="blog-thumb aos-item" data-aos="fade-down">
                                    <img src="<?php  the_post_thumbnail_url(); ?>" alt="blog" class="img-responsive">
                                    <div class="blog-date btm-left"><span><?php the_time('m'); ?> </span><?php the_time('M') ?></div>
                                </div>
                                <div class="blog-content  p60 aos-item" data-aos="fade-up">
                                    <h5><?php the_title(); ?></h5>
                                    <ul class="list-inline blog-list">
                                        <li><i class="fa fa-folder"></i> Anyhink,Business </li>
                                        <li><i class="fa fa-user"></i> <?php the_author(); ?></li>
                                    </ul>
                                    <p><?php echo wp_trim_words(get_the_content(),15,false); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="read-more">Read More  <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>

            </div>

            <div class="row">
                <div class="col-md-12 text-center aos-item" data-aos="fade-up">
                    <a href="<?php echo esc_url(home_url('/blog')); ?>" class="btn bizacafe-black ">view more posts <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </section>

<?php }
add_shortcode('bizlatestpost','bizlatestpost_shortcode');







