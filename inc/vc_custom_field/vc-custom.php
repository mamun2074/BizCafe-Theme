<?php

/*vc map custom*/

function bizcafe_vc_map(){


    /*Portfolio*/
    vc_map(array(
        'base'=>'bizcafeportfolioshortcode',
        'name'=>'BizCafe Portfolio',
        'description'=>'Post portfolio',
        'icon'=>get_template_directory_uri().'/img/Portfolio.png',
        'params'=>array(
            array(
                'param_name'=>'number',
                'type'=>'textfield',
                'description'=>'Number of post you want to show',
                'heading'=>'Post Number',
            ),
        ),


    ));

    /*Counter */

    vc_map(array(
        'base'=>'biz_counter',
        'name'=>'Timear',
        'description'=>'This area is satisfaction and count number',
        'icon'=>get_template_directory_uri().'/img/countert.png',
        'params'=>array(
            array(
                'param_name'=>'satisfied_counter',
                'type'=>'textfield',
                'heading'=>'Clint satisfaction number',
                'description'=>'This area is satisfication clients',
            ),
            array(
                'param_name'=>'satisfied_clint_text',
                'type'=>'textfield',
                'heading'=>'Clint satisfaction text',
                'description'=>'This area is satisfication clients text',
            ),
            array(
                'param_name'=>'monthly_counter',
                'type'=>'textfield',
                'heading'=>'Monthly earning count',
                'description'=>'Monthly earning count number',
            ),
            array(
                'param_name'=>'monthly_counter_text',
                'type'=>'textfield',
                'heading'=>'Monthly earning count text',
                'description'=>'Monthly earning count description',
            ),
             array(
                'param_name'=>'professonal_counter',
                'type'=>'textfield',
                'heading'=>'Projessonal result number',
                'description'=>'Professonal result count',
            ),
             array(
                'param_name'=>'professonal_text',
                'type'=>'textfield',
                'heading'=>'Projessonal result number text',
                'description'=>'Professonal result count text description',
            ),

             array(
                'param_name'=>'project_complete_counter',
                'type'=>'textfield',
                'heading'=>'Project complete count',
                'description'=>'Project complete number',
            ),
             array(
                'param_name'=>'project_complete_text',
                'type'=>'textfield',
                'heading'=>'Project complete count text',
                'description'=>'Project complete description',
            ),


        ),
    ));

/*Our team*/

    vc_map(array(
        'base'=>'bizourteam',
        'name'=>'BizCafe Our team',
        'description'=>'Post our team',
        'icon'=>get_template_directory_uri().'/img/team.png',
        'params'=>array(
            array(
                'param_name'=>'team_header',
                'type'=>'textfield',
                'heading'=>'Team header top',
                'description'=>'Write team header top title',
            ),
             array(
                'param_name'=>'team_title',
                'type'=>'textfield',
                'heading'=>'Team title',
                'description'=>'Write team title',
            ),
            array(
                'param_name'=>'team_description',
                'type'=>'textfield',
                'heading'=>'Team description',
                'description'=>'Write team description',
            ),
            array(
                'param_name'=>'post_per_page',
                'type'=>'textfield',
                'heading'=>'Show team member',
                'description'=>'How many team member you show?',
            ),



        ),
    ));




    vc_map(array(

       'base'=>'bizlatestpost',

        'name'=>'BizCafe latest post',

        'description'=>'Latest post',

        'icon'=>get_template_directory_uri().'/img/blogger.png',

        'params'=>array(

            array(
                'param_name'=>'team_header',
                'type'=>'textfield',
                'heading'=>'Team header top',
                'description'=>'Write team header top title',
            ),
            array(
                'param_name'=>'team_title',
                'type'=>'textfield',
                'heading'=>'Team title',
                'description'=>'Write team title',
            ),
            array(
                'param_name'=>'team_description',
                'type'=>'textfield',
                'heading'=>'Team description',
                'description'=>'Write team description',
            ),
            array(
                'param_name'=>'post_per_page',
                'type'=>'textfield',
                'heading'=>'Show team member',
                'description'=>'How many team member you show?',
            ),

    )));





}
add_action('vc_before_init','bizcafe_vc_map');