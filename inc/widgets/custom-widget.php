<?php



/*Sidebar Widget */

/*popular */

class popular extends WP_Widget{
    function __construct()
    {

        $popular=array(
          'description'=>'Biz recent post',
        );
        parent::__construct('popularpost', 'BizCafe Popular Post',$popular);
    }
    function widget($args, $instance)
    {
        $title=$instance['title'];
        $count=$instance['count_post'];
        ?>

        <?php echo $args['before_widget']; ?>
            <?php echo $args['before_title']; ?><?php echo $title; ?><?php echo $args['after_title']; ?>


        <?php

        $recent_post=new WP_Query(array(
            'post_type'=>'post',
            'posts_per_page'=>$count,
        ));

        while ($recent_post->have_posts()):$recent_post->the_post(); ?>

            <article class="post post-list media ">
                <div class="small-post-thumbnail">
                    <a href="#">
                        <img src="<?php the_post_thumbnail_url(); ?>" class="media-object" alt="post-img">
                    </a>
                </div>
                <div class="post-body">
                    <a href="#" class="post-title"><?php the_title(); ?></a>
                    <p class="post-date"><?php the_time('d M'); ?></p>
                </div>
            </article>

        <?php endwhile; ?>

        <?php echo $args['after_widget']; ?>

    <?php }
    function form($instance)
    {
        $title=$instance['title'];
        $count=$instance['count_post'];

        ?>
        <label for="<?php echo  $this->get_field_id('title'); ?>">Title</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" class="widefat">

        <label for="<?php echo $this->get_field_id('postnumber'); ?>">Post number</label>
        <input type="number" class="widefat" value="<?php echo $count; ?>" name="<?php echo $this->get_field_name('count_post'); ?>" id="<?php echo $this->get_field_id('postnumber'); ?>">


   <?php }
}




function bizCafe_popular_post(){

    register_widget('popular');

}
add_action('widgets_init','bizCafe_popular_post');


/*Footer widget*/

/*recent post*/


class recent_post extends WP_Widget{
    function __construct()
    {
        $widget_options=array(
            'description'=>'It is footer recent post',
        );
        parent::__construct('biz_recent_post', 'BizCafe Recent post', $widget_options);
    }
    function widget($args, $instance)
    {
        $title=$instance['title'];
        $number=$instance['number'];


        ?>

        <?php echo $args['before_widget']; ?>
                <?php echo $args['before_title']; ?><?php echo $title; ?><?php echo $args['after_title']; ?>


        <?php
        $recnet_post_footer=new WP_Query(array(
            'post_type'=>'post',
            'posts_per_page'=>$number,
        ));

        while ($recnet_post_footer->have_posts()):$recnet_post_footer->the_post(); ?>
                 <div class="widget-content">
                    <div class="date"><span><?php the_time('d'); ?></span> <?php the_time('M'); ?></div>
                    <div class="text">
                        <h5><?php the_title(); ?></h5>
                        <p class="comment">
                            <i class="fa fa-comment"></i> <?php comments_popup_link('0','1','%'); ?>
                        </p>
                    </div>
                </div>
        <?php endwhile; ?>

        <?php echo $args['after_widget']; ?>


    <?php }
    function form($instance)
    {
        $title=$instance['title'];
        $number=$instance['number'];

        ?>

        <label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
        <input type="text" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" >


        <label for="<?php echo $this->get_field_id('number'); ?>">Count post</label>

        <input type="number" class="widefat" id="<?php echo  $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $number; ?>" >

   <?php  }
}


function bizCafe_recent_post(){
    register_widget('recent_post');

}
add_action('widgets_init','bizCafe_recent_post');



/*Contact info*/



class bizContactInfo extends WP_Widget{

   function  __construct()
   {
       $id_base='biz_info';
       $name='BizContactInfo';
       $widget_options=array(
         'description'=>'Contact info of widget',
       );
       parent::__construct($id_base, $name, $widget_options);
   }
   function widget($args, $instance)
   {

       $title=$instance['title'];
       $location=$instance['location'];
       $office=$instance['office'];
       $email=$instance['email'];
       $fax=$instance['fax'];
       ?>

       <div class="col-md-3 col-sm-6">
           <div class="footer-widget widget-contact">
               <h4 class="widget-title"><?php echo $title; ?></h4>
               <div class="widget-content">

                   <ul>
                       <li>
                           <i class="fa fa-map-marker"></i>
                           <p><?php echo $location; ?></p>
                       </li>
                       <li>
                           <i class="fa fa-phone"></i>
                           <p><?php echo $office; ?></p>
                       </li>
                       <li>
                           <i class="fa fa-envelope"></i>
                           <p><?php echo $email; ?></p>
                       </li>
                       <li>
                           <i class="fa fa-fax"></i>
                           <p>FAX : <?php echo $fax; ?></p>
                       </li>
                   </ul>

               </div>
           </div>
       </div>

        <?php
    }

   function form($instance)
   {
      $title= $instance['title'];
      $location= $instance['location'];
      $office= $instance['office'];
      $email= $instance['email'];
       $fax= $instance['fax'];

       ?>

       <label for="<?php echo $this->get_field_id('title'); ?>">Title</label>

       <input type="text" class="widefat" id="<?php echo  $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" >


       <label for="<?php echo $this->get_field_id('location'); ?>">Location</label>

       <input type="text" class="widefat" id="<?php echo $this->get_field_id('location'); ?>" name="<?php echo $this->get_field_name('location'); ?>" value="<?php echo $location; ?>">


       <label for="<?php echo $this->get_field_id('office'); ?>">Office name</label>

       <input type="text" class="widefat" id="<?Php echo $this->get_field_id('office');  ?>" name="<?php echo $this->get_field_name('office'); ?>" value="<?php echo $office; ?>" >


       <label for="<?php echo $this->get_field_id('email'); ?>">Valid Email</label>

       <input type="email" class="widefat" id="<?php echo $this->get_field_id('email');  ?>" name="<?php echo $this->get_field_name('email'); ?>" value="<?php echo $email; ?>" >

       <label for="<?php echo $this->get_field_id('fax');  ?>">Fax</label>

       <input type="text" class="widefat" id="<?php echo $this->get_field_id('fax'); ?>" name="<?php echo $this->get_field_name('fax'); ?>" value="<?php echo $fax; ?>" >
   <?php }

}


function bizcafe_contact_info(){

    register_widget('bizContactInfo');

}
add_action('widgets_init','bizcafe_contact_info');


/*Photo Gallery*/

class bizPhoto extends WP_Widget{
    function __construct()
    {
        $id_base='bizcafewidget';
        $name="BizCafePhotoGallery ";
        $widget_options=array(
                'description'=>'BizCafeGallery Widget only footer',
        );

        parent::__construct($id_base, $name, $widget_options);
    }
    function widget($args, $instance)
    {

        $title=$instance['title'];
        $number=$instance['number'];
       ?>
        <?php echo $args['before_widget'];?>
                <?php echo $args['before_title']; ?><?php echo $title; ?><?php echo $args['after_title']; ?>
                <div class="widget-content">
                    <ul class="list-inline">

                        <?php

                       $custom_gal= new WP_Query(array(
                                'post_type'=>'bizgallery',
                                 'posts_per_page'=>$number,
                        ));


                        while ($custom_gal->have_posts()):$custom_gal->the_post(); ?>

                        <li>
                            <a href="#"><img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive"></a>
                        </li>

                         <?php endwhile; ?>



                    </ul>
                </div>
            <?php echo $args['after_widget']; ?>

        <?php
    }
    function form($instance)
    {
        $title=$instance['title'];
        $number=$instance['number'];
       ?>

        <label for="<?php echo  $this->get_field_id('title'); ?>">Title</label>

        <input type="text" id="<?php echo  $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" echo $title; >


        <label for="<?php echo  $this->get_field_id('number'); ?>">Number of photos</label>

        <input type="number" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $umber; ?>" class="widefat";  >

        <?php
    }
}


function BizPhontGallery(){
    register_widget('bizPhoto');
}
add_action('widgets_init','BizPhontGallery');



