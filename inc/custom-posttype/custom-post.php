<?php

/*Gallery*/
    register_post_type('bizgallery',array(
        'public'=>true,
        'labels'=>array(
            'name'=>'Gallery',
            'add_new'=>'Add gallery items',
        ),
        'menu_position'=>5,
        'menu_icon'=>'dashicons-format-gallery',
        'supports'=>array('title','thumbnail'),
    ));


    /*project */
      register_post_type('bizportfolio',array(
        'public'=>true,
        'labels'=>array(
            'name'=>'Portfolio',
            'add_new'=>'Add Portfolio items',
        ),
        'menu_position'=>5,
        'menu_icon'=>get_template_directory_uri().'/img/dashboard.png',
        'supports'=>array('title','thumbnail','editor'),
    ));


      /*Testimonial*/

   register_post_type('testimonial',array(
        'public'=>true,
        'labels'=>array(
            'name'=>'Testimonial',
            'add_new'=>'Add testimonial items',
        ),
        'menu_position'=>5,
        'menu_icon'=>get_template_directory_uri().'/img/testidash.png',
        'supports'=>array('title','thumbnail','editor'),
    ));

   /*Our team*/

    register_post_type('ourteam',array(
        'public'=>true,
        'labels'=>array(
            'name'=>'Our Team',
            'add_new'=>'Add tem member',
        ),
        'menu_position'=>5,
        'menu_icon'=>get_template_directory_uri().'/img/teamdash.png',
        'supports'=>array('title','thumbnail','editor'),
    ));




