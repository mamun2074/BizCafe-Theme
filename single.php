<?php get_header(); ?>

<!--SECTION PAGE HEADER START-->
<section id="section-page-header" class="page-blog">
    <div class="overlay black"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="page-header text-center">
                    <h3>BLOG</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<!--SECTION PAGE HEADER END-->

<!--SECTION BLOG  START-->

<div id="blog-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-7">


                <?php while (have_posts()):the_post();  ?>
                <article class="blog-standard">
                    <div class="post-thumbnail">
                        <img src="<?php the_post_thumbnail_url();?>" alt="" class="img-responsive">
                        <div class="blog-date right-top"><span><?php  the_time('d')?></span><?php the_time('D') ?></div>
                    </div>
                    <div class="blog-article-details">
                        <h4><?php the_title(); ?></h4>
                        <p class="post-comments">
                            <a href="#"><i class="fa  fa-folder-open"></i><?php the_category(' '); ?></a>
                            <a href="#"><i class="fa  fa-user"></i><?php the_author(); ?></a>
                        </p>
                        <hr>
                        <p><?php the_content(); ?></p>

                        <div class="post-share-widget">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Share this post:</h4>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list-inline">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>

                    <!--     BLOG COMMENTS  START  -->
                    <div class="blog-comments">
                        <h3 class="comment-heading"><?php comments_popup_link('No comment','One comment','% Comments'); ?></h3>
                        <div class="media comment-wrap">
                            <?php comments_template(); ?>
                        </div>

                </article>
                <?php endwhile; ?>


            </div>

            <!--       SIDEBAR     -->
            <div class="col-md-4 col-sm-4">
              <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>
<!--SECTION BLOG END-->


<?php get_footer(); ?>