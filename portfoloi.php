<?php

/*
 * Template Name:Portfolio
 */


get_header();



while (have_posts()):the_post();

    $value=get_post_meta(get_the_ID(),'header_banner',true);

endwhile; ?>

<?php if ($value==1): ?>

    <!--SECTION PAGE HEADER START-->

    <section id="section-page-header" class="page-blog">
        <div class="overlay black"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-header text-center">
                        <?php while (have_posts()):the_post(); ?>
                            <h3><?php the_title(); ?></h3>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>

    <!--SECTION Portfolio  START-->

    <section id="section-portfolio" class="bizcafe-content-padding page-portfolio">
        <div class="container">
            <div class="row">

                <?php
                $bizportfolio=new WP_Query(array(
                    'post_type'=>'bizportfolio',
                    'posts_per_page'=>9,
                ));

                while($bizportfolio->have_posts()):$bizportfolio->the_post();

                   get_post_meta(get_the_ID(),'portfoliodesignation',true);

                ?>
                <div class="col-md-4 col-sm-4">
                    <div class="portfolio-block-wrap">
                        <div class="portfolio-content-block">
                            <a href="<?php the_post_thumbnail_url(); ?>" class="hover-overlay work-popup">
                                <figure class="overlay-effect">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
                                </figure>
                                <div class="portfolio-caption">
                                    <h5><?php echo get_post_meta(get_the_ID(),'portfoliodesignation',true); ?></h5>
                                    <p>Business/ Marketing</p>
                                </div>
                            </a>
                        </div>
                        <div class="portfolio-content">
                            <h3><?php the_title(); ?></h3>
                            <h4><?php echo wp_trim_words(get_the_content(),25,false); ?></h4>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>

    <!--SECTION Portfolio END-->

<?php get_footer(); ?>