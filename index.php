<?php get_header(); ?>

    <!--SECTION PAGE HEADER START-->

    <section id="section-page-header" class="page-blog">
        <div class="overlay black"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="page-header text-center">
                        <h3>BLOG</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!--SECTION PAGE HEADER END-->

    <!--SECTION BLOG  START-->

    <div id="blog-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7">

                    <?php while (have_posts()):the_post() ?>

                    <article class="blog-standard">
                        <div class="post-thumbnail">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
                            <div class="blog-date right-top"><span><?php the_time('d') ?></span><?php the_time('M') ?></div>
                        </div>
                        <div class="blog-article-details">
                            <h4><?php the_title(); ?></h4>
                            <p class="post-comments">
                                <a href="#"><i class="fa  fa-folder-open"></i><?php the_category(' '); ?></a>
                                <a href="#"><i class="fa  fa-user"></i><?php the_author(); ?></a>
                            </p>
                            <hr>
                            <p><?php echo wp_trim_words(get_the_content(),50,true); ?></p>
                            <a href="<?php the_permalink(); ?>" class="read-more">Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </article>

                    <?php endwhile; ?>

                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2 </a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                </div>

                <div class="col-md-4 col-sm-4">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
    <!--SECTION BLOG END-->

<?php get_footer(); ?>