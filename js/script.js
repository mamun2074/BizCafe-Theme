

jQuery(function ($) {
    "use strict";
    
         /* ----------------------------------------------------------- */
	/*  Fixed header
	/* ----------------------------------------------------------- */

	$(window).on('scroll', function(){
		if ( $(window).scrollTop() > 70 ) {
			$('.site-navigation, .header-white, .header').addClass('navbar-fixed');
		} else {
			$('.site-navigation, .header-white, .header').removeClass('navbar-fixed');
		}
	});
    
    
      jQuery(document).on('ready', function(){   
          
          

	/* ----------------------------------------------------------- */
	/*  Mobile Menu
	/* ----------------------------------------------------------- */

	jQuery(".nav.navbar-nav li a").on("click", function() { 
		jQuery(this).parent("li").find(".dropdown-menu").slideToggle();
		jQuery(this).find("i").toggleClass("fa-angle-down fa-angle-up");
	});



        /* ----------------------------------------------------------- */
        /*  MAin Slider
        /* ----------------------------------------------------------- */

        $('.slider-carousel').owlCarousel({
            items: 1,
            nav: true,
            slideSpeed: 6000,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            pagination: false,
            singleItem: true,
            navText: ["", ""]

        });



   

          
          /*START GOOGLE MAP*/
		function initialize() {
		  var mapOptions = {
			zoom: 15,
			scrollwheel: false,
			center: new google.maps.LatLng(40.7127837, -74.00594130000002)
		  };
		  var map = new google.maps.Map(document.getElementById('map'),
			  mapOptions);
		  var marker = new google.maps.Marker({
			position: map.getCenter(),
			icon: 'assets/img/map_pin.png',
			map: map
		  });
		}
		google.maps.event.addDomListener(window, 'load', initialize);	
		/*END GOOGLE MAP*/	

          
  

});



/*----------------- -on Load function ------------------*/
$(window).on('load', function () {

    
      $('.progress-bar').each(function () {
                var each_bar_width = $(this).attr('aria-valuenow');
                $(this).width(each_bar_width + '%');
            });
    
     /* ----------------------------------------------------------- */
     /*  Preloader
    /* ----------------------------------------------------------- */
    
    $(".loader").delay(1000).fadeOut("slow");
    $(".content_name .hello").addClass('animated zoomIn');
    $(".content_name .name").addClass('animated zoomIn');
    $(".content_prof p").addClass('animated zoomIn');
    $(".content_download p").addClass('animated zoomIn');
    
      /* ----------------------------------------------------------- */
        /*  Video popup
        /* ----------------------------------------------------------- */
       

       $(".popup").colorbox({
                iframe: true,
                innerWidth: 650,
                innerHeight: 400
            });
    
        // Bootstrap ToolTip
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'manual'
        }).tooltip('show');




     /*
     * ----------------------------------------------------------------------------------------
     *  MAGNIFIC POPUP JS
     * ----------------------------------------------------------------------------------------
     */

    var magnifPopup = function () {
        $('.work-popup').magnificPopup({
            type: 'image',
            removalDelay: 300,
            mainClass: 'mfp-with-zoom',
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function (openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    };
    // Call the functions 
    magnifPopup();

    
    
//    Counter 
    
  $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

});


//    Animation
AOS.init({
    duration: 1500,
});
            
 
	/* ----------------------------------------------------------- */
	/*  Main slideshow home3
	/* ------------------ ----------------------------------------- */

		$('#main-slide').carousel({
			pause: true,
			interval: 100000,
		});

    

});
