<?php

function BizCafe_general()
{

    add_theme_support('title-tag');
    add_theme_support('menus');
    add_theme_support('thumbnails');
    add_theme_support('custom-background');
    add_theme_support('custom-header');
    add_theme_support('widgets');

    register_nav_menus(array(
        'mein_menu' => "Main Menu",

    ));


    /*sidebar area include*/

    include_once 'inc/sidebar/widget-sidebar.php';

    /*Custom post type*/

    include_once 'inc/custom-posttype/custom-post.php';

}

add_action('after_setup_theme', 'BizCafe_general');

/*Walker include*/
include_once 'walker-nav-menu.php';

/*cmb2 include*/
include_once 'inc/cmb2/init.php';
include_once 'inc/cmb2/bizcmb.php';

/*Custom widget include*/

include_once 'inc/widgets/custom-widget.php';


/*inlcude style and js*/
function bizcafe_style(){

    wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.min.css');
    wp_enqueue_style('awesome',get_template_directory_uri().'/css/font-awesome.min.css');
    wp_enqueue_style('magnific',get_template_directory_uri().'/css/magnific-popup.css');
    wp_enqueue_style('carousel',get_template_directory_uri().'/css/owl.carousel.min.css');
    wp_enqueue_style('animate',get_template_directory_uri().'/css/animate.min.css');
    wp_enqueue_style('theme',get_template_directory_uri().'/css/theme.css');
    wp_enqueue_style('responsive',get_template_directory_uri().'/css/responsive.css');
    wp_enqueue_style('stylesheet',get_stylesheet_uri());



    wp_enqueue_script('jquery');

    wp_enqueue_script('bootstrap',get_template_directory_uri().'/js/bootstrap.min.js',array('jquery'),true,true);

    wp_enqueue_script('colorbox',get_template_directory_uri().'/js/jquery.colorbox.js',array('jquery'),true,true);

    wp_enqueue_script('magnific',get_template_directory_uri().'/js/jquery.magnific-popup.min.js',array('jquery'),true,true);

    wp_enqueue_script('waypoints',get_template_directory_uri().'/js/jquery.waypoints.min.js',array('jquery'),true,true);

    wp_enqueue_script('counterup',get_template_directory_uri().'/js/jquery.counterup.min.js',array('jquery'),true,true);

    wp_enqueue_script('carousel',get_template_directory_uri().'/js/owl.carousel.js',array('jquery'),true,true);

    wp_enqueue_script('easing',get_template_directory_uri().'/js/jquery.easing.1.3.js',array('jquery'),true,true);

    wp_enqueue_script('aos',get_template_directory_uri().'/js/aos.js',array('jquery'),true,true);

    wp_enqueue_script('aosscript',get_template_directory_uri().'/js/script.js',array('jquery'),true,true);

}
add_action('wp_enqueue_scripts','bizcafe_style');


/*Include shortcode and visual composer*/
include_once 'inc/biz-shortcode/biz-shortcode.php';

/*vc map include*/
include_once 'inc/vc_custom_field/vc-custom.php';




























