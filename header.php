<!DOCTYPE html>
<html <?php language_attributes(); ?>>


<!-- Mirrored from themeturn.com/pro/db/biz/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 May 2017 08:58:03 GMT -->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.<?php echo get_template_directory_uri(); ?>/js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>


<body <?php body_class(); ?> id="" data-spy="scroll" data-target=".navbar-default">

<section id="header-top-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <ul class="list-inline top-contact">
                    <li><i class="fa fa-phone"></i> 20034578134</li>
                    <li><i class="fa fa-envelope"></i>bizstart.com creative @@</li>
                    <li><i class="fa fa-map-marker"></i>123 Jackson, New York</li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-5 text-right">
                <ul class="list-inline top-social">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa  fa-vimeo-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa  fa-google"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>




<header id="masthead" class="site-navigation navigation">
    <nav id="siteHeaderNavbar" class="navbar navbar-default">
        <!-- Navigation -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse"> <i class="fa fa-bars"></i> </button>
                        <a class="navbar-brand page-scroll " href="<?php bloginfo('home'); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="" class="img-responsive"> </a>
                    </div>
                </div>

                <div class="col-md-9 col-sm-12">
                    <div class="collapse navbar-collapse navbar-right navbar-main-collapse navigation-menu">

                        <?php
                        wp_nav_menu(array(
                            'theme_location'=>'mein_menu',
                            'menu_class'=>'nav navbar-nav',
                            'walker'=>new bizCafe_Walker(),

                        ));

                        ?>

                    </div>

                    <!--Search Box-->
                  <!--  <div class="search-box-outer">
                        <div class="dropdown">
                            <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                            <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                <li class="panel-outer">
                                    <div class="form-container">
                                        <form method="post" action="#">
                                            <div class="form-group">
                                                <input type="search" name="field-name" value="" placeholder="Search Here">
                                                <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <!-- /.container -->

</header>
<!-- .site-header-navbar-->
